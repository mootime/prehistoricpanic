﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using HutongGames.PlayMaker;

public class CharacterMovement : MonoBehaviour
{
    CharacterController characterController;
    public CameraFollow CameraFollow_script;
    public float playerspeed = 20.0f;
    public Vector3 inputvector;
    public Vector3 camvector;
    public Animator anim_controller;
    public ParticleSystem particlesys;
    public bool moving = false;

    AudioSource fxPlayer;

    // Start is called before the first frame update
    void Start()
    {
      characterController = GetComponent<CharacterController>();
      CameraFollow_script = GameObject.Find("/CameraBase").GetComponent<CameraFollow>();
      anim_controller = GameObject.Find("UnityUltiTest").GetComponent<Animator>();
      particlesys = GameObject.Find("Particle_Dirt").GetComponent<ParticleSystem>();
      fxPlayer = GetComponent<AudioSource>();   
    }

    // Update is called once per frame
    void Update()
    {
    
    anim_controller.SetFloat("speed", inputvector.magnitude); 

    if(Input.GetAxis("Horizontal") > 0.1 || Input.GetAxis("Vertical") > 0.1){

        particlesys.enableEmission = true;
        moving = true;
        //fxPlayer.clip = Resources.Load<AudioClip>("SFX/footstep_dirt_walk_run_02");
        //fxPlayer.PlayOneShot(Resources.Load<AudioClip>("SFX/footstep_dirt_walk_run_02"));

    }
    else{

      moving = false;
      particlesys.enableEmission = false;

    }

    //Calculate movement from input and align it with cameras rotation
    inputvector = new Vector3(Input.GetAxis("Horizontal"), 0, Input.GetAxis("Vertical"));
    inputvector = Vector3.ClampMagnitude(inputvector, 1);
    camvector = CameraFollow_script.localRotation * inputvector;
    camvector.y = 0;
     

    //Get speed from speedcalc fsm
    playerspeed = FsmVariables.GlobalVariables.GetFsmFloat("PlayerSpeed").Value;

    //apply speed and move!
    camvector *= playerspeed;

    characterController.Move(camvector * Time.deltaTime);
    //Slap some gravity on too
    characterController.Move(new Vector3 (0,-12, 0) * Time.deltaTime);

      //failsafe incase player lets up but unity input does not detect it
      //inputvector = Vector3.zero;
      //camvector = Vector3.zero;

    }
}
