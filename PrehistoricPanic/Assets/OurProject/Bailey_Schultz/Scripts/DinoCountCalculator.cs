﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DinoCountCalculator : MonoBehaviour
{

    public GameObject[] howmanyarmor;
    public GameObject[] howmanyspeed;
    private bool plzwait = false;

    void Start(){

        howmanyarmor = GameObject.FindGameObjectsWithTag("ArmorDino");
        howmanyspeed = GameObject.FindGameObjectsWithTag("SpeedDino");
        Debug.Log("At Scene start:  " + howmanyarmor.Length + " Armor Dinos" + " in scene.");
        Debug.Log("At Scene start: " + howmanyspeed.Length + " Speed Dinos" + " in scene.");

    }

    // Update is called once per frame
    void FixedUpdate()
    {
        if(plzwait == false){
            plzwait = true;
            StartCoroutine(wait());
        }
    }

    IEnumerator wait(){

        yield return new WaitForSeconds(10);
        howmanyarmor = GameObject.FindGameObjectsWithTag("ArmorDino");
        howmanyspeed = GameObject.FindGameObjectsWithTag("SpeedDino");
        plzwait = false;
        Debug.Log("Currently " + howmanyarmor.Length + " Armor Dinos" + " in scene.");
        Debug.Log("Currently " + howmanyspeed.Length + " Speed Dinos" + " in scene.");
        
    }
    }