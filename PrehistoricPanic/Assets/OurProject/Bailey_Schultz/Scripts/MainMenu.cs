﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using TMPro;
using HutongGames.PlayMaker;

public class MainMenu : MonoBehaviour
{

    public Text bottomlinetext;
    public TextMeshProUGUI bottomlinetextpro;

    public int lvl_num = 0;
    public string lvl_to_load;
    public Text leveltext;
    public TextMeshProUGUI leveltextpro;

    public bool p_mode = false;

    // Start is called before the first frame update
    void Start()
    {
        if(p_mode == false){
     //leveltext = GameObject.Find("LS_BLT").GetComponent<Text>();
        leveltextpro = GameObject.Find("LS_BLT").GetComponent<TextMeshProUGUI>();
        }
    }

    // Update is called once per frame
    void Update()
    {

        if(p_mode == false){
        Debug.Log("case");
        switch (lvl_num){

            case 0:
            lvl_to_load = "Level_Adam";
            //leveltext.text = "Forest";
            leveltextpro.SetText("Level 1 - Forest");
            GameObject.Find("lvl_image").GetComponent<Image>().sprite = Resources.Load<Sprite>("UI/lvl1_temp");

            break;

            case 1:
            lvl_to_load = "Level_Michael";
            //leveltext.text = "Monsoon";
            leveltextpro.SetText("Level 2 - Badlands");
            GameObject.Find("lvl_image").GetComponent<Image>().sprite = Resources.Load<Sprite>("UI/lvl2_final");

            break;

            case 2:
            lvl_to_load = "Level_Qui";
            //leveltext.text = "BadLands";
            leveltextpro.SetText("Level 3 - Monsoon");
            GameObject.Find("lvl_image").GetComponent<Image>().sprite = Resources.Load<Sprite>("UI/lvl3_final");

            break;

            case 3:
            lvl_to_load = "Level_Bailey";
            //leveltext.text = "Volcano";
            leveltextpro.SetText("Level 4 - Rocky Ridge");
            GameObject.Find("lvl_image").GetComponent<Image>().sprite = Resources.Load<Sprite>("UI/lvl4_temp");

            break;


        }

        }

    }

    public void textget (string current_text){

        //bottomlinetext = GameObject.Find(current_text).GetComponent<Text>();
        bottomlinetextpro = GameObject.Find(current_text).GetComponent<TextMeshProUGUI>();

    }

    public void textset (string desc){

        //bottomlinetext.text = desc;
        bottomlinetextpro.SetText(desc);

    }

    public void load_canvas (string canvas_name){

        //Debug.Log(GameObject.Find(canvas_name));
        GameObject.Find("/" + canvas_name).GetComponent<Canvas>().enabled = true;

    }

    public void getcanvas (string current_canvas){

         GameObject.Find("/" + current_canvas).GetComponent<Canvas>().enabled = false;

    }

    public void startgame (){
    
        SceneManager.LoadScene("Level_Adam");
    
    }

    public void quitgame (){
    
        Application.Quit();
    
    }

    public void load_ui_image (string image_name){

        if(GameObject.Find(image_name).GetComponent<Image>().enabled == true){

        GameObject.Find(image_name).GetComponent<Image>().enabled = false;

        }else{

          GameObject.Find(image_name).GetComponent<Image>().enabled = true;  

        }

    }

    public void load_ui_canvas (string image_name){

        if(GameObject.Find(image_name).GetComponent<Canvas>().enabled == true){

        GameObject.Find(image_name).GetComponent<Canvas>().enabled = false;

        }else{

          GameObject.Find(image_name).GetComponent<Canvas>().enabled = true;  

        }

    }

    //lvl select

    public void right_arrow(){

        if(lvl_num < 3){
            lvl_num ++;
        }
        else{

            lvl_num = 0;

        }

    }
    
    public void left_arrow(){

        if(lvl_num > 0){
            lvl_num --;
        }
        else{
            lvl_num = 3;
        }

    }

    public void load_level(){

        SceneManager.LoadScene(lvl_to_load);

    }

    public void load_level_manual(string level){

        SceneManager.LoadScene(level);

    }

    public void resume(){

        GetComponent<Canvas>().enabled = false;
        Time.timeScale = 1.0f;
        PlayMakerGUI.LockCursor = true;
        PlayMakerGUI.HideCursor = true;

    }

    public void retry(){
        Time.timeScale = 1.0f;
        Scene scene = SceneManager.GetActiveScene();
        SceneManager.LoadScene(scene.name);

    }

    public void nextlevel(){
        //Why did unity switch to this fucking shit?

       int indexnum = SceneUtility.GetBuildIndexByScenePath(SceneManager.GetActiveScene().ToString());

        if(indexnum == 4){

            SceneManager.LoadScene("MainMenu");
            
        }
        else{

       indexnum ++;
       SceneManager.LoadScene(indexnum);

    }

    }

}


