﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Pause : MonoBehaviour
{

    AudioSource fxPlayer;
    public AudioClip pause_noise;

    public bool pmode = true;

    // Start is called before the first frame update
    void Start()
    {
     fxPlayer = GetComponent<AudioSource>();   
    }

    // Update is called once per frame
    void Update()
    {

    if(Input.GetButtonDown("Pause") && Time.timeScale != 0.0f && pmode == true){
        fxPlayer.PlayOneShot(pause_noise);
        Debug.Log("Game Paused");
        GetComponent<Canvas>().enabled = true;
        Time.timeScale = 0.0f;

    }else if(Input.GetButtonDown("Pause") && Time.timeScale == 0.0f && pmode == true){

        GetComponent<Canvas>().enabled = false;
        Time.timeScale = 1.0f;

    }

    }

    void FixedUpdate(){

    if(GetComponent<Canvas>().enabled == true && pmode == false){

       StartCoroutine(wait());

    }

    if(GetComponent<Canvas>().enabled == false){

       

    }
    
    }

    IEnumerator wait(){

        yield return new WaitForSeconds(2);
        Time.timeScale = 0.0f;

    }
}
