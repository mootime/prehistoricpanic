﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//get yer eyeballs outta here ya bum (⌐▀͡ ̯ʖ▀)=/̵͇̿̿/'̿'̿̿̿ ̿ ̿̿
public class Spawner : MonoBehaviour
{
    [Tooltip("Enter either ArmorDino or SpeedDino.")]
    public string tagtype;
    [Tooltip("Enter the amount of dinos that must be left in the scene for spawner to engage.")]
    public int amount_cutoff = 10;
    [Tooltip("Amount of Dinos you want to spawn.")]
    public int amount_tospawn = 5;
    private bool tripped = false;

    public DinoCountCalculator DCC_script;

    void Start(){

       DCC_script = GameObject.Find("/DCC").GetComponent<DinoCountCalculator>();

    }

    void FixedUpdate(){   

        /*if(howmany.Length <= amount_cutoff && tripped == false){

            Instantiate(Resources.Load("prefab/SpawnBoom"), transform.position, transform.rotation);

            if(tagtype == "ArmorDino"){
                for(int i = 0; i <= amount_tospawn; i++){
                    Instantiate(Resources.Load("prefab/Armor_Dino"), transform.position, transform.rotation); 
                }
                tripped = true;

            }
            else{
                for(int i = 0; i <= amount_tospawn; i++){
                    Instantiate(Resources.Load("prefab/Speed_Dino"), transform.position, transform.rotation); 
                }
                tripped = true;

            }

        }*/

        if(tagtype == "ArmorDino" && DCC_script.howmanyarmor.Length <= amount_cutoff && tripped == false){

            Instantiate(Resources.Load("prefab/SpawnBoom"), transform.position, transform.rotation);

            for(int i = 0; i <= amount_tospawn; i++){
                Instantiate(Resources.Load("prefab/Armor_Dino"), transform.position, transform.rotation); 
            }
                tripped = true; 

        }

        if(tagtype == "SpeedDino" && DCC_script.howmanyspeed.Length <= amount_cutoff && tripped == false){

            Instantiate(Resources.Load("prefab/SpawnBoom"), transform.position, transform.rotation);

            for(int i = 0; i <= amount_tospawn; i++){
                Instantiate(Resources.Load("prefab/Speed_Dino"), transform.position, transform.rotation); 
            }
                tripped = true; 

        }

        if(tagtype == "ArmorDino" && DCC_script.howmanyarmor.Length > amount_cutoff){

            tripped = false;

        }

        if(tagtype == "ArmorDino" && DCC_script.howmanyarmor.Length > amount_cutoff){

            tripped = false;

        }

    }
}
