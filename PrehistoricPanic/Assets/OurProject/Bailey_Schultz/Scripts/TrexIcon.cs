﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TrexIcon : MonoBehaviour
{

    public Vector3 screenpos;
    public Vector3 screencenter;
    public Vector3 direction;

    public float movex;
    public float movey;
    public Vector3 movevect;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {

        screenpos = Camera.main.WorldToScreenPoint(GameObject.Find("DEBUG_REX").transform.position);
        //transform.GetComponent<RectTransform>().localPosition = screenpos;

        //transform.GetComponent<RectTransform>().localPosition = new Vector3(0,1000,0);
        
        //transform.position = Vector3.RotateTowards(GameObject.Find("Player_Test").transform.position, GameObject.Find("DEBUG_REX").transform.position,360 * Mathf.Deg2Rad * Time.deltaTime, 1);


        screencenter = new Vector3(1920, 1080, 0)/2;
        screenpos -= screencenter;

        float angle = Mathf.Atan2(screenpos.y, screenpos.x);
        angle -= 90 *Mathf.Deg2Rad;

        float cos = Mathf.Cos(angle);
        float sin = -Mathf.Sin(angle);

        screenpos = screencenter + new Vector3(sin*150, cos*150, 0);

        float m = cos/sin;

        Vector3 screenbounds = screencenter * 0.9f;

        //up down check
        if(cos>0){
            screenpos = new Vector3(screenbounds.y/m, screenbounds.y, 0);
        }else{
            screenpos = new Vector3(-screenbounds.y/m, -screenbounds.y, 0);
        }
        //bounds check

        if(screenpos.x > screenbounds.x){
            screenbounds = new Vector3(screenbounds.x, screenbounds.x*m,0);
       }else if(screenpos.x < -screenbounds.x){
       screenpos = new Vector3(-screenbounds.x, - screenbounds.x*m,0);
       }

       screenpos += screencenter;

       transform.GetComponent<RectTransform>().localPosition = screenpos;

        //movex = screenpos.x;
        //movey = screenpos.y;

        //movex = Mathf.Clamp(movex, -960, 960);
        //movey = Mathf.Clamp(movey, -540, 540);
        //movevect = new Vector3(movex, movey, 0);

        //transform.GetComponent<RectTransform>().localPosition = movevect;


    }
}
